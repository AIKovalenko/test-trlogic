<?php

class StringHelper
{

    /**
     * @param $string
     * @return string
     */
    public static function removeXss($string)
    {
        $string = strip_tags($string);
        $string = htmlentities($string, ENT_QUOTES, 'UTF-8');
        return htmlspecialchars($string, ENT_QUOTES);
    }
}