<?php

return [
    'title' => 'TR Logic LLC Панель управления',
    'Dashboard' => 'Панель управления',
    'Products' => 'Товары',
    'Orders' => 'Заказы',
    'Customers' => 'Клиенты',
    'Reports' => 'Отчеты',
    'Integrations' => 'Интеграции',
    'Languages' => 'Языки',
    'ru' => 'Русский',
    'en' => 'Английский',
    'Sign out' => 'Выйти',
    'Signin title' => 'Добро пожаловать в нашу панель управления',
    'Email address' => 'Ваш Email',
    'Password' => 'Пароль',
    'Remember me' => 'Запомнить меня',
    'Sign in' => 'Авторизоваться',
    'Sign up' => 'Зарегистрироваться',
    'Name' => 'Ваше имя',
    'Surname' => 'Фамилия',
    'Photo' => 'Фото',
    'Field name is required' => 'Поле Имя является обязательным!',
    'Field surname is required' => 'Поле Фамилия является обязательным!',
    'Field email is required' => 'Поле email является обязательным!',
    'Field password is required' => 'Поле Пароль является обязательным!',
    'Field photo is required' => 'Поле Фото является обязательным!',
    'Email is not corrected' => 'Поле Email не корректно',
    'Can only upload jpeg, png, gif' => 'Не поддерживаемый тип файла, можно загружать только jpeg, png, gif',
    'Minimum length password 5 symbols' => 'Минимальная длина пароля 5 символов',
    'This email is already registered' => 'Данный email уже зарегистрирован',
    'This email not found' => 'Данный email не найден',
    'File not upload' => 'Не возможно загрузить файл',
    'You have successfully registered' => 'Вы успешно зарегистрировались',
    'Profile' => 'Персональная страница',
];
