<?php

abstract class Form
{

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var array
     */
    protected $data;

    protected $errors = [];

    public function __construct(Application $app, array $data)
    {
        $this->app = $app;
        $this->data = $data;

        $this->setAttributes();
    }

    abstract public function validate();
    abstract public function run();

    private function setAttributes()
    {
        foreach ($this->data as $attr => $value) {
            if (property_exists($this, $attr)) {
                $this->$attr = $value;
            }
        }
    }

    /**
     * @param string $text
     */
    protected function addError($text)
    {
        array_push($this->errors, $text);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function isValidate()
    {
        return !$this->errors;
    }
}