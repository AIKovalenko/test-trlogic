<?php

include_once 'Form.php';
include_once 'helpers/StringHelper.php';

class SignIn extends Form
{

    public $email;
    public $password;
    public $remembermy;

    public function validate()
    {
        foreach (['email', 'password'] as $field) {
            if (empty($this->$field)) {
                $this->addError($this->app->translate("Field $field is required"));
            }
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->addError($this->app->translate('Email is not corrected'));
        } else {
            $connect = $this->app->getMysqli();
            $email = StringHelper::removeXss($this->email);
            $res = $connect->query("SELECT id FROM users WHERE email = '$email' LIMIT 1");
            $row = $res->fetch_assoc();
            if (empty($row)) {
                $this->addError($this->app->translate('This email not found'));
            }
        }

        if (strlen($this->password) < 5) {
            $this->addError($this->app->translate('Minimum length password 5 symbols'));
        }

        return true;
    }

    public function run()
    {
        $connect = $this->app->getMysqli();

        $email = StringHelper::removeXss($this->email);
        $password = md5(StringHelper::removeXss($this->password));

        $res = $connect->query("SELECT email FROM users WHERE email='$email' AND password='$password' LIMIT 1");
        $row = $res->fetch_assoc();

        if ($row) {
            $_SESSION['login'] = $email;
        }

        return $res;
    }
}