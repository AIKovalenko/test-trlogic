<?php

include_once 'Form.php';
include_once 'helpers/StringHelper.php';
include_once 'helpers/FileHelper.php';

class SignUp extends Form
{

    public $name;
    public $surname;
    public $email;
    public $password;
    public $photo;
    public $remembermy;

    /**
     * @return bool
     */
    public function validate()
    {
        foreach (['name', 'surname', 'email', 'password'] as $field) {
            if (empty($this->$field)) {
                $this->addError($this->app->translate("Field $field is required"));
            }
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->addError($this->app->translate('Email is not corrected'));
        } else {
            $connect = $this->app->getMysqli();
            $email = StringHelper::removeXss($this->email);
            $res = $connect->query("SELECT id FROM users WHERE email = '$email' LIMIT 1");
            $row = $res->fetch_assoc();
            if (!empty($row)) {
                $this->addError($this->app->translate('This email is already registered'));
            }
        }

        if (!empty($_FILES['Signup'])) {
            $mime = [
                'image/png',
                'image/jpeg',
                'image/jpeg',
                'image/jpeg',
                'image/gif',
            ];

            $imageInfo = getimagesize($_FILES['Signup']['tmp_name']['photo']);

            if (!$imageInfo || !in_array($imageInfo['mime'], $mime)) {
                $this->addError($this->app->translate('Can only upload jpeg, png, gif'));
            } else {

                FileHelper::createDir('uploads');

                $uploadFile = 'uploads/' . basename($_FILES['Signup']['name']['photo']);

                if (!move_uploaded_file($_FILES['Signup']['tmp_name']['photo'], $uploadFile)) {
                    $this->addError($this->app->translate('File not upload'));
                }

                $this->photo = $uploadFile;
            }
        }

        if (strlen($this->password) < 5) {
            $this->addError($this->app->translate('Minimum length password 5 symbols'));
        }


        return true;
    }

    /**
     * @return bool|mysqli_result
     */
    public function run()
    {
        $connect = $this->app->getMysqli();

        $name = StringHelper::removeXss($this->name);
        $surname = StringHelper::removeXss($this->surname);
        $email = StringHelper::removeXss($this->email);
        $photo = StringHelper::removeXss($this->photo);
        $password = md5(StringHelper::removeXss($this->password));

        $res = $connect->query("INSERT INTO users(name, surname, email, password, photo) VALUES ('$name', '$surname', '$email', '$password', '$photo')")
        or trigger_error("Query Failed! SQL Error: " . mysqli_error($connect), E_USER_ERROR);

        if ($res) {
            $_SESSION['success'] = $this->app->translate('You have successfully registered');
            $_SESSION['login'] = $email; // authorise user
        }

        return $res;
    }
}