<?php

include_once 'forms/SignUp.php';
include_once 'forms/SignIn.php';

class Application
{

    const LANGUAGE_EN = 'en';
    const LANGUAGE_RU = 'ru';

    private static $app;

    /**
     * @var string
     */
    private $language;

    /**
     * @var string
     */
    private $defaultLanguage = self::LANGUAGE_EN;

    /**
     * @var array
     */
    private $languages = [
        self::LANGUAGE_EN,
        self::LANGUAGE_RU,
    ];

    private $i18n = [];

    /**
     * @var Form
     */
    public $form;

    /**
     * @var mysqli
     */
    private $mysqli;

    /**
     * @return Application
     */
    public static function init()
    {
        if (self::$app) {
            return self::$app;
        }

        session_start();

        self::$app = new self();
        self::$app->initMysql();
        self::$app->setLanguage();

        self::$app->isLogout();
        self::$app->isSignup();
        self::$app->isSignin();

        return self::$app;
    }

    /**
     * @return string
     */
    private function setLanguage()
    {
        if (empty($this->i18n)) {
            foreach ($this->languages as $lang) {
                $this->i18n[$lang] = include(sprintf('i18n/%s.php', $lang));
            }
        }

        if (!empty($_GET['language']) && in_array($_GET['language'], $this->languages, true)) {
            $this->language = $_GET['language'];
            setcookie('language', $this->language, time() + 3600, '/');
        } elseif (!empty($_COOKIE['language']) && in_array($_COOKIE['language'], $this->languages, true)) {
            $this->language = $_COOKIE['language'];
        } else {
            $this->language = $this->defaultLanguage;
        }

        return $this->language;
    }

    /**
     * @return array
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param $text
     * @return string
     */
    public function translate($text)
    {
        if (!empty($this->i18n[$this->language][$text])) {
            return $this->i18n[$this->language][$text];
        } elseif (!empty($this->i18n[$this->defaultLanguage][$text])) {
            return $this->i18n[$this->defaultLanguage][$text];
        } else {
            return $text;
        }
    }

    /**
     * @return bool
     */
    public function isAuthorise()
    {
        return !empty($_SESSION['login']);
    }

    private function isLogout()
    {
        if (isset($_GET['logout'])) {
            session_destroy();
            unset($_SESSION['login']);
            $this->refresh();
        }
    }

    /**
     * @return string
     */
    public function getView()
    {
        $pages = [
            'signin' => true,
            'signup' => true,
            'dashboard' => false,
            'orders' => false,
            'profile' => false,
        ];

        if (!empty($_GET['page']) && in_array($_GET['page'], array_keys($pages), true)) {
            if (!$pages[$_GET['page']] && !$this->isAuthorise()) {
                return 'views/signin.php';
            }
            return sprintf('views/%s.php', $_GET['page']);
        } elseif ($this->isAuthorise()) {
            return 'views/dashboard.php';
        }

        return 'views/signin.php';
    }

    /**
     * @return bool
     */
    private function isSignup()
    {
        if (empty($_POST['Signup'])) {
            return false;
        }

        $signUp = $this->form = new SignUp($this, $_POST['Signup']);
        if ($signUp->validate() && $signUp->isValidate()) {
            try {
                if ($signUp->run()) {
                    $this->refresh();
                }
            } catch (Exception $exception) {
                //to log
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function isSignin()
    {
        if (empty($_POST['Signin'])) {
            return false;
        }

        $signIn = $this->form = new SignIn($this, $_POST['Signin']);

        if ($signIn->validate() && $signIn->isValidate()) {
            try {
                if ($signIn->run()) {
                    $this->refresh();
                }
            } catch (Exception $exception) {
                //to log
            }

            return true;
        }

        return false;
    }

    private function refresh()
    {
        header('location: index.php');
    }

    /**
     * @throws Exception
     */
    private function initMysql()
    {
        $this->mysqli = new mysqli('mysql', 'trlogic_user', 'trlogic-passwd-cigoltr', 'trlogic');
        if ($this->mysqli->connect_errno) {
            throw new Exception('Не удалось подключиться к MySQL: (' . $this->mysqli->connect_errno . ') ' . $this->mysqli->connect_error);
        }
    }

    /**
     * @return mysqli
     */
    public function getMysqli()
    {
        return $this->mysqli;
    }


    /**
     * @return array|bool|null
     */
    public function getUser()
    {
        if (!$this->isAuthorise()) {
            return false;
        }

        $connect = $this->getMysqli();
        $email = StringHelper::removeXss($_SESSION['login']);
        $res = $connect->query("SELECT * FROM users WHERE email = '$email' LIMIT 1");
        $row = $res->fetch_assoc();

        return $row;
    }


}