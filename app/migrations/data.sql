-- Adminer 4.7.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `trlogic`;
CREATE DATABASE `trlogic` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `trlogic`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `surname` varchar(90) NOT NULL,
  `password` varchar(40) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `name_surname` (`name`,`surname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 2020-01-19 14:26:40