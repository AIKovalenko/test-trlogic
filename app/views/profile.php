<? if ($user = $app->getUser()): ?>
    <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col-auto d-none d-lg-block">
            <img src="<?= $user['photo']?>"  width="250" height="250" alt="<?= $user['name']?>">
        </div>
        <div class="col p-4 d-flex flex-column position-static">
            <h3 class="mb-0"><?= $user['name']?> <?= $user['surname']?></h3>
            <p class="mt-1"><?= $user['email']?></p>
        </div>
    </div>
<? endif; ?>
