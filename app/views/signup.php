<?
/**
 * @var $form SignUp
 */
$form = $app->form
?>

<form class="form-signin" method="post" enctype="multipart/form-data">
    <div class="text-center mb-4">
        <img class="mb-4" src="./assets/logo.jpeg" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal"><?= $app->translate('Signin title') ?></h1>
        <? if ($app->form && !$app->form->isValidate()): ?>
            <? foreach ($app->form->getErrors() as $error): ?>
                <code><?= $error?></code>
            <? endforeach; ?>
        <? endif; ?>
    </div>

    <div class="form-label-group">
        <input type="email" id="inputEmail" name="Signup[email]" class="form-control"
               placeholder="<?= $app->translate('Email address') ?>" required="required"
               value="<?= $form && $form->email ? $form->email : '' ?>"
               autofocus="">
        <label for="inputEmail"><?= $app->translate('Email address') ?></label>
    </div>

    <div class="form-label-group">
        <input type="text" id="inputName" name="Signup[name]" class="form-control"
               placeholder="<?= $app->translate('Name') ?>" required="required"
               value="<?= $form && $form->name ? $form->name : '' ?>"
               autofocus="">
        <label for="inputName"><?= $app->translate('Name') ?></label>
    </div>

    <div class="form-label-group">
        <input type="text" id="inputSurname" name="Signup[surname]" class="form-control"
               placeholder="<?= $app->translate('Surname') ?>" required="required"
               value="<?= $form && $form->surname ? $form->surname : '' ?>"
               autofocus="">
        <label for="inputSurname"><?= $app->translate('Surname') ?></label>
    </div>

    <div class="form-label-group">
        <input type="password" id="inputPassword" name="Signup[password]" class="form-control"
               placeholder="<?= $app->translate('Password') ?>"
               value="<?= $form && $form->password ? $form->password : '' ?>"
               required="required">
        <label for="inputPassword"><?= $app->translate('Password') ?></label>
    </div>

    <div class="form-label-group">
        <input type="file" id="inputPhoto" name="Signup[photo]" class="form-control-file"
               placeholder="<?= $app->translate('Photo') ?>"
               required="required">
        <label for="inputPhoto"><?= $app->translate('Photo') ?></label>
    </div>

    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" name="Signup[remembermy]" value="remember-me"> <?= $app->translate('Remember me') ?>
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit"><?= $app->translate('Sign up') ?></button>
    <p class="mt-2 mb-2 text-center"><a href="/?page=signin"><?= $app->translate('Sign in') ?></a></p>
    <p class="mt-2 text-muted text-center">© 2017-<?= date('Y') ?></p>
</form>