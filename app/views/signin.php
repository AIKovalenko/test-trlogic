<?
/**
 * @var $form SignIn
 */
$form = $app->form
?>
<form class="form-signin" method="post">
    <div class="text-center mb-4">
        <img class="mb-4" src="./assets/logo.jpeg" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal"><?= $app->translate('Signin title') ?></h1>

        <? if ($app->form && !$app->form->isValidate()): ?>
            <? foreach ($app->form->getErrors() as $error): ?>
                <code><?= $error?></code>
            <? endforeach; ?>
        <? endif; ?>
    </div>

    <div class="form-label-group">
        <input type="email" id="inputEmail" name="Signin[email]" class="form-control" placeholder="<?= $app->translate('Email address') ?>" required=""
               value="<?= $form && $form->email ? $form->email : '' ?>"
               autofocus="">
        <label for="inputEmail"><?= $app->translate('Email address') ?></label>
    </div>

    <div class="form-label-group">
        <input type="password" id="inputPassword" name="Signin[password]" class="form-control" placeholder="<?= $app->translate('Password') ?>"
               value="<?= $form && $form->password ? $form->password : '' ?>"
               required="">
        <label for="inputPassword"><?= $app->translate('Password') ?></label>
    </div>

    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> <?= $app->translate('Remember me') ?>
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit"><?= $app->translate('Sign in') ?></button>
    <p class="mt-2 mb-2 text-center"><a href="/?page=signup"><?= $app->translate('Sign up') ?></a></p>
    <p class="mt-2 text-muted text-center">© 2017-<?=date('Y')?></p>
</form>