<?
include_once('Application.php');

$app = Application::init();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?= $app->translate('title') ?></title>
    <!-- Bootstrap core CSS -->
    <link href="./assets/bootstrap.min.css" rel="stylesheet">
    <link href="./assets/dashboard.css" rel="stylesheet">
    <link href="./assets/signin.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/">TR Logic LLC</a>

    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <? if ($app->isAuthorise()): ?>
                <a class="nav-link" href="/?logout"><?= $app->translate('Sign out') ?></a>
            <? else: ?>
                <a class="nav-link" href="/">Sign In</a>
            <? endif; ?>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link <?= !empty($_GET['page']) && $_GET['page'] === 'dashboard' ? 'active' : '' ?>"
                           href="/?page=dashboard"> <span data-feather="home"></span>
                            <?= $app->translate('Dashboard') ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?= !empty($_GET['page']) && $_GET['page'] === 'orders' ? 'active' : '' ?>"
                           href="/?page=orders"> <span data-feather="file"></span>
                            <?= $app->translate('Orders') ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/?page=products">
                            <span data-feather="shopping-cart"></span>
                            <?= $app->translate('Products') ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/?page=customers">
                            <span data-feather="users"></span>
                            <?= $app->translate('Customers') ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/?page=reports">
                            <span data-feather="bar-chart-2"></span>
                            <?= $app->translate('Reports') ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/?page=integrations">
                            <span data-feather="layers"></span>
                            <?= $app->translate('Integrations') ?>
                        </a>
                    </li>
                </ul>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span><?= $app->translate('Languages') ?></span>
                </h6>

                <ul class="nav flex-column mb-2">
                    <? foreach ($app->getLanguages() as $lang) : ?>
                        <li class="nav-item">
                            <a class="nav-link <?= $app->getLanguage() === $lang ? 'active' : '' ?>"
                               href="/?language=<?= $lang ?>">
                                <span data-feather="file-text"></span>
                                <?= $app->translate($lang) ?>
                            </a>
                        </li>
                    <? endforeach; ?>
                </ul>

                <? if ($app->isAuthorise()): ?>
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <a href="/?page=profile"><span><?= $app->translate('Profile') ?></span></a>
                    </h6>
                <? endif; ?>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

            <? if (!empty($_SESSION['success'])): ?>
                <div class="alert alert-success" role="alert">
                    <?
                    echo $_SESSION['success'];
                    unset($_SESSION['success']);
                    ?>
                </div>
            <? endif; ?>

            <?= include_once($app->getView()); ?>
        </main>
    </div>
</div>


<!-- Bootstrap core JavaScript
========================================= ========= -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>

<script src="./assets/popper.min.js"></script>
<script src="./assets/bootstrap.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

</body>
</html>
